# MCPU assembler, see https://github.com/cpldcpu/MCPU
#   python asm.py <test.asm >test.obj

from __future__ import print_function
import sys

def valof(arg):
    if arg in syms:
        arg = syms[arg]
    try:
        return int(arg)
    except:
        return 0

# pseudo ops
def org(arg):
    global pc
    pc = valof(arg)
def dcb(arg):
    global pc
    mem[pc & 0x3F] = valof(arg)
    pc += 1

# instruction set
def nor(arg): dcb(valof(arg) | 0x00)
def add(arg): dcb(valof(arg) | 0x40)
def sta(arg): dcb(valof(arg) | 0x80)
def jcc(arg): dcb(valof(arg) | 0xC0)

# combined instructions
def jcs(arg): jcc(pc+2); jcc(arg)
def jmp(arg): jcc(arg); jcc(arg)
def lda(arg): nor('allone'); add(arg)
def sub(arg): nor('zero'); add(arg); add('one')
def out(arg): dcb(0xFF)

code = sys.stdin.readlines() # read a line on the input file
syms = {} 

for _ in range(2):
    pc = 0                          # Program counter, 
    mem = 64 * [0]                  # Simulates the memory
    for line in code:               # foreach line on the input file
        fields = line.split()       # Cuts in few parts
        if len(fields) > 0 and line[0] != '#': # If the line is not empty and if is not a comment
            if line.lstrip() == line: # no spaces until the first caracter -> there is a label
                syms[fields[0]] = pc  # Copy of pc corresponding to the label
                del fields[:1]        # del of the rest of the line
            f = globals()[fields[0]] 
            f(fields[1])

s = '\n'
print(s.join([format(x, '02X') for x in mem])) # print the compile code 