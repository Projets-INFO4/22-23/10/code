// This program is a compiler that will convert Small-C language
// into an asm assembly program made for a MCPU processor.
// @see https://github.com/cpldcpu/MCPU
// for more informations.
// This program will be auto compiled by the CPU
// thus explaining the implentation choice.
// This program was made for educational purposes feel
// free to modify and update this code at your will
// @authors Brun Samuel, Jordan Kemgne Nasah Darryl ,Lithaud Alexandre, Tyndal Lucas



#include <stdio.h>
#include <stdlib.h>

/*=================== READABLE LETTERS =====================*/
/*================ COMPARISON LETTERS ==============*/
#define NOT 33                          // !
#define OR 124                          // |
#define AND 38                          // &

#define EQUALS 61                       // =
#define LESSTHAN 60                     // <
#define MORETHAN 62                     // >
/*=============== ARITHMETIC LETTERS ===============*/
#define PLUS 43                         // +
#define MINUS 45                        // -
#define MODULO 37                       // %
/*================== CODE LETTERS ==================*/
#define OPEN_PARENTHESIS 40             // (
#define CLOSE_PARENTHESIS 41            // )

#define OPEN_BRACKET 123                // {
#define CLOSE_BRACKET 125               // }

#define OPEN_SQUARE_BRACKET 91          // [
#define CLOSE_SQUARE_BRACKET 93         // ]

#define SIMPLE_QUOTE 39                 // '
#define DOUBLE_QUOTE 34                 // "

#define COMA 44                         // , 
#define SEMICOLON 59                    // ;
#define POINT 46                        // .
/*=============== REJECTED LETTERS ================*/
#define TABULATION 9
#define SPACE 32
#define NEWLINE 10
#define CARRIAGE_RETURN 13
/*================= OTHER LETTERS ==================*/
#define HASH 35                         // #
#define STAR 42                         // *
#define SLASH 47                        // /
#define BACK_SLASH_ZERO 0               // /0
/*========================================================*/

/*========================= TOKENS ===========================*/
#define IDENT_TOKEN 0                   // nom de vari

/* == KEYWORDS == */
#define IF_TOKEN 10                     // if
#define ELSE_TOKEN 11                   // else
#define DO_TOKEN 12                     // do
#define WHILE_TOKEN 13                  // while
#define INT_TOKEN 14                    // int
#define CHAR_TOKEN 15                   // char
#define RETURN_TOKEN 16                 // return
#define DEFINE_TOKEN 17                 // #define
// TODO : add break, continue, (switch) and update define to get functions

/* == Punctuations == */
#define OPEN_PAR_TOKEN 30               // (
#define CLOSE_PAR_TOKEN 31              // )
#define OPEN_BRACK_TOKEN 32             // {
#define CLOSE_BRACK_TOKEN 33            // }
#define OPEN_SQUARE_BRACK_TOKEN 34      // [
#define CLOSE_SQUARE_BRACK_TOKEN 35     // ]
#define SEMICOLON_TOKEN 36              // ;
#define DOT_TOKEN 37                    // .

/* == OPERATORS == */
#define ADD_TOKEN 50                    // +
#define SUB_TOKEN 51                    // -
#define DIV_TOKEN 52                    // /
#define MOD_TOKEN 53                    // %
#define STAR_TOKEN 54                   // *
#define ADR_TOKEN 55                    // &

#define NOT_TOKEN 60                    // ! 
#define AND_TOKEN 61                    // &&
#define OR_TOKEN 62                     // || 

#define EQ_TOKEN 70                     // ==
#define NEQ_TOKEN 71                    // !=
#define LESS_TOKEN 72                   // <
#define LESS_EQ_TOKEN 73                // <=
#define MORE_TOKEN 74                   // >
#define MORE_EQ_TOKEN 75                // >=

#define ASSIGN_TOKEN 80                  // =

/* == LITERALS == */
#define INT_LITERAL_TOKEN 90            // entiers
#define STRING_LITERAL_TOKEN 91         // String
#define CHAR_LITERAL_TOKEN 92           // Char

/* == OTHERS == */
#define ERROR_TOKEN 32765               //raise when an error has been detected
#define EOF_TOKEN 32766                 // show the EOF
/*========================================================*/

/*====================OTHER CONSTANTS======================*/
#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1

#define _INITFILE FILE* file = 0

#define _INITOUTFILE FILE* outfile = 0
//#define _PUTOUTFILE(s) (fprintf(outfile,"%s",s)) // Not working because we will need to recursive parmetres in printf (but I tried)

#define _GET (fgetc(file))              // Enables to get the next character from file
/*========================================================*/

/*========================== LEXER DEFINITIONS & FUNCTIONS =======================*/
/* Init the global file variable */
_INITFILE;
_INITOUTFILE;

#define LEXER_BUFFER_SIZE 128
int lexerFileBuffer[LEXER_BUFFER_SIZE]; // table of LEXER_BUFFER_SIZE characters for the file 
int getIndex = LEXER_BUFFER_SIZE; //position of the readable index in the table (the first non-readed)

// This variable are use for debugs ended
int fileCharCount = 0;          // Number of characters in the whole file
int fileLineCount = 0;          // Line number in the file
int byLineCharCount = 0;        // Show the line number of characteres

#define TOKEN_VALUE_SIZE 128
char tokenCharValue[TOKEN_VALUE_SIZE]; // table for the result of the tokenization. Contains the value for the current token and the end of string character
int tokenIntValue;

/**
 * It copies the non read part of the buffer to the beginning of the buffer and fills the rest of the
 * buffer with new characters
 * 
 * WARNING : This function do not update the global file counter because they are updated in the bellows functions
 */
void fillLexerBuffer(){
    int i = 0;
    //copy non read lexerFileBuffer parts in the beginning of the table
    while(getIndex < LEXER_BUFFER_SIZE){
        //copy the end of the table
        lexerFileBuffer[i] = lexerFileBuffer[getIndex];
        //update position
        getIndex = getIndex + 1;

        i = i + 1;
        
    }
    //reset of the getting index
    getIndex = 0;
    //fill the table with new characters
    while(i<LEXER_BUFFER_SIZE){
        // read the next caractere in the open file (input)
        int c = _GET;
        if(c == -1){ break; } //if the eof is find
        lexerFileBuffer[i] = c;
        i = i + 1;
        
    }

    //if eof, fill with eof character
    while(i<LEXER_BUFFER_SIZE){
        lexerFileBuffer[i] = EOF_TOKEN;
        i = i + 1;
    }
}

/**
 * If it's impossible to see forward (from getIndex), fill the buffer, then return the next character.
 * 
 * @return The next character in the buffer.
 */
int lookNextChar(){
    if(getIndex < 0 || getIndex >= LEXER_BUFFER_SIZE){ //if it's impossible to see forward (from getIndex)
        fillLexerBuffer(); //fill
    }
    //return the next characters
    return lexerFileBuffer[getIndex];
}

/**
 * If it's impossible to see forward (from getIndex), fill the buffer, then return the next character.
 * 
 * @return The next character in the buffer.
 */
int lookNextCharN(int n){
    if(n >= LEXER_BUFFER_SIZE) {return ERROR_TOKEN;}
    if(getIndex < 0 || (getIndex + n) >= LEXER_BUFFER_SIZE){ //if it's impossible to see forward (from getIndex)
        fillLexerBuffer(); //fill
    }
    //return the next characters
    return lexerFileBuffer[getIndex + n];
}

/**
 * This function gets the next character from the input file and updates the counters
 * 
 * @return The next character in the file.
 */
int getNextChar(){
    //get the next char
    int c = lookNextChar();
    //update the get counter (the new last readed)
    getIndex = getIndex + 1;

    //Counters updates
    fileCharCount = fileCharCount + 1;
    byLineCharCount = byLineCharCount + 1; // one more in the line

    if(c == NEWLINE) { // if newline, reset by line and update fileLineCount
        fileLineCount = fileLineCount + 1;
        byLineCharCount = 0;
    }
    return c;
}

/**
 * It returns 1 if the character is a tabulation, a space, a newline or a carriage return
 * 
 * @param c the character to be checked
 * 
 * @return The function is_skippable returns a boolean value.
 */
int is_skippable(int c){
    return c == TABULATION || c == SPACE || c == NEWLINE || c == CARRIAGE_RETURN;
}

/**
 * `end_of_line_char` returns true if the character is a newline or a carriage return.
 * 
 * @param c The character to check.
 * 
 * @return A boolean value.
 */
int end_of_line_char(int c){
    return c == NEWLINE || c == CARRIAGE_RETURN || c == EOF_TOKEN;
}

/**
 * It skips spaces
 * 
 * @param nextChar the next character to be read
 * 
 * @return The next character that is not a space.
 */
int skip_spaces(int nextChar){
    //compute and return the next non-spaces characters
    int newNext = nextChar;
    while(is_skippable(newNext)){
        newNext = getNextChar(); //read the next one
    }
    return newNext;
}

/**
 * It checks if the next character is the start of a comment, and if it is, it skips the comment and
 * returns the next character after the comment
 * 
 * @param nextChar the next character to be read
 * 
 * @return The next character to be considered.
 */
int comments_check(int nextChar){
    // look if it's a comments start with //
    if(nextChar == SLASH && lookNextChar() == SLASH ){
        // it's a comment, we have to skip
        int newNext;
        do {
            newNext = getNextChar();
        }while(!end_of_line_char(newNext));//while there is no end of line, the comment continue
        // when we read the end of line, we have to check spaces and again the comments
        return comments_check(skip_spaces(newNext));
    }
    // look if it's a comments start with /*
    else if(nextChar == SLASH && lookNextChar() == STAR ){
        // it's a comment, we have to skip
        int newNext;
        do {
            newNext = getNextChar();
        }while((newNext != EOF_TOKEN) && !(newNext == STAR && lookNextChar() == SLASH));
        //if the output condition is not newNext == EOF_TOKEN
        //we have to skip the 2 characters * and /
        if(newNext != EOF_TOKEN){
            newNext = getNextChar(); // new next is now the /
            newNext = getNextChar(); // new next is now the real new who have to be considarate
        }else{
            //If there is no end symbole (*/) in the file, raise an ERROR_TOKEN
            return ERROR_TOKEN;
        }
        // when we read the end of comment, we have to check spaces and again the comments
        return comments_check(skip_spaces(newNext));
    }
    return nextChar;
}

/**
 * It skips the spaces and comments and returns the next character that is not a space or a comment
 * 
 * @return The next character that is not a space or a comment.
 */
int skip_skippable(){
    //skip the spaces
    int nextChar; 
    do{
        nextChar = getNextChar();
        //printf("D : %c\n",nextChar);
    } while(is_skippable(nextChar));;

    // skip the potential comments
    return comments_check(nextChar);
}

/**
 * It gets the next character and checks if it's a number. If it is, compute the int value and retry.
 * 
 * @param nextChar the current character
 */
void get_number(int nextChar){ //the first place in tokenValue will contain the current number
    int result = nextChar - '0'; // get the real value by removing the ascii value
    int potNext = lookNextChar();
    while(potNext >= '0' && potNext <= '9'){// while the next is an alpha number
        result = (result * 10) + (getNextChar() - '0'); // compute the int value
        potNext = lookNextChar();
    }

    //save the result 
    tokenIntValue = result;
}

/**
 * It gets the next character, checks if it's an EOF, saves it, gets the next character, checks if it's
 * a single quote, and returns the token
 * 
 * @param nextChar the next character to be read from the input stream
 * 
 * @return The token type
 */
int get_char(int nextChar){
    int c = getNextChar(); // get the char
    if(c == EOF_TOKEN){ return ERROR_TOKEN;} // check if ok
    tokenCharValue[0] = c; //save  
    c = getNextChar(); //check if the form is 'c'
    if(c != SIMPLE_QUOTE){ return ERROR_TOKEN; }
    return CHAR_LITERAL_TOKEN;
}

/**
 * The function get_string() reads characters from the input file until it encounters a double quote
 * character. 
 * 
 * The characters read are stored in the tokenCharValue array. 
 * 
 * The function returns the token type STRING_LITERAL_TOKEN
 * 
 * @param nextChar the next character in the input stream
 * 
 * @return The token type of the string literal
 */
int get_string(int nextChar){
    //get the first string character
    nextChar = getNextChar();
    int i = 0;
    //will we didnt see the end of string "
    while(nextChar != DOUBLE_QUOTE){
        //if error by eof
        if(nextChar == EOF_TOKEN) { return ERROR_TOKEN; }
        //if error by seg fault
        if(i == TOKEN_VALUE_SIZE - 1) { return ERROR_TOKEN; }
        //save token
        tokenCharValue[i] = nextChar;
        //update and consume one
        nextChar = getNextChar();
        i = i + 1;
    }
    // check if the line is empty
    if(i == 0) return ERROR_TOKEN;
    //end, consume the " by the last getNestChar of the loop
    tokenCharValue[i] = BACK_SLASH_ZERO;
    return STRING_LITERAL_TOKEN;
}

/**
 * It returns true if the character is a letter or a number
 * 
 * @param c the character to be checked
 */
int can_be_contain_in_name(int c){
    // true if 0-9, a-z or A-Z
    return (c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z');
}

/**
 * It checks if the next character is the start of a keyword, and if it is, it consumes the rest of the
 * keyword and returns the token for that keyword
 * 
 * @param nextChar the next character to be read
 * 
 * @return The token type of the keyword.
 */
int reconize_keywords(int nextChar){
    //detect an IF statement
    if(nextChar == 'i' && lookNextCharN(0) == 'f' && (!can_be_contain_in_name(lookNextCharN(1)) || lookNextCharN(1) == OPEN_PAR_TOKEN)){
        getNextChar();//consume the f
        return IF_TOKEN;
    }
    //detect else
    if(nextChar == 'e' && lookNextCharN(0) == 'l' && lookNextCharN(1) == 's' && lookNextCharN(2) == 'e' && (!can_be_contain_in_name(lookNextCharN(3)) || lookNextCharN(3) == OPEN_BRACK_TOKEN)){
        getNextChar();//consume the l
        getNextChar();//consume the s
        getNextChar();//consume the e
        return ELSE_TOKEN;
    }
    //detect do
    if(nextChar == 'd' && lookNextCharN(0) == 'o' && (!can_be_contain_in_name(lookNextCharN(1)) || lookNextCharN(1) == OPEN_BRACK_TOKEN)){
        getNextChar();//consume the o
        return DO_TOKEN;
    }
    //detect while
    if(nextChar == 'w' && lookNextCharN(0) == 'h' && lookNextCharN(1) == 'i' && lookNextCharN(2) == 'l' && lookNextCharN(3) == 'e' && (!can_be_contain_in_name(lookNextCharN(4)) || lookNextCharN(4) == OPEN_PAR_TOKEN)){
        getNextChar();//consume the h
        getNextChar();//consume the i
        getNextChar();//consume the l
        getNextChar();//consume the e
        return WHILE_TOKEN;
    }
    //detect int
    if(nextChar == 'i' && lookNextCharN(0) == 'n' && lookNextCharN(1) == 't' && !can_be_contain_in_name(lookNextCharN(2))){
        getNextChar();//consume the n
        getNextChar();//consume the t
        return INT_TOKEN;
    }
    //detect char
    if(nextChar == 'c' && lookNextCharN(0) == 'h' && lookNextCharN(1) == 'a' && lookNextCharN(2) == 'r' && !can_be_contain_in_name(lookNextCharN(3))){
        getNextChar();//consume the h
        getNextChar();//consume the a
        getNextChar();//consume the r
        return CHAR_TOKEN;
    }
    //detect return
    if(nextChar == 'r' && lookNextCharN(0) == 'e' && lookNextCharN(1) == 't' && lookNextCharN(2) == 'u' && lookNextCharN(3) == 'r' && lookNextCharN(4) == 'n' && !can_be_contain_in_name(lookNextCharN(5))){
        getNextChar();//consume the e
        getNextChar();//consume the t
        getNextChar();//consume the u
        getNextChar();//consume the r
        getNextChar();//consume the n
        return RETURN_TOKEN;
    }
    //detect #define
    if(nextChar == '#' && lookNextCharN(0) == 'd' && lookNextCharN(1) == 'e' && lookNextCharN(2) == 'f' && lookNextCharN(3) == 'i' && lookNextCharN(4) == 'n' && lookNextCharN(5) == 'e' && !can_be_contain_in_name(lookNextCharN(6))){
        getNextChar();//consume the d
        getNextChar();//consume the e
        getNextChar();//consume the f
        getNextChar();//consume the i
        getNextChar();//consume the n
        getNextChar();//consume the e
        return DEFINE_TOKEN;
    }
    //no correspondances
    return -1;
}

/**
 * > It reads the next character and checks if it is a keyword. If it is, it returns the keyword token.
 * If it is not, it reads the next character and checks if it is a skippable character. If it is, it
 * returns the string literal token. If it is not, it continues.
 * 
 * @param nextChar the next character to be read
 * 
 * @return The token type
 */
int reconize_ID(int nextChar){
    int tmp = reconize_keywords(nextChar); //check the keywords
    if(tmp != -1){ return tmp;} // if detected

    //else
    tokenCharValue[0] = nextChar;
    // at least 1 char else the flow not goes here

    int i = 1;
    // until skippable char
    while(can_be_contain_in_name(lookNextChar()) || lookNextChar() == EOF_TOKEN){
        //if eof, we consume the current string
        if(lookNextChar() == EOF_TOKEN) { return IDENT_TOKEN; } 
        //if error by seg fault
        if(i == TOKEN_VALUE_SIZE - 1) { return ERROR_TOKEN; }
        
        //update and consume one, no eof here
        nextChar = getNextChar();
        //save token
        tokenCharValue[i] = nextChar;
        
        i = i + 1;
    }
    //if the caractere is not reconized
    if(i==1 && !can_be_contain_in_name(nextChar)){ return ERROR_TOKEN; }
    //end of string
    tokenCharValue[i] = BACK_SLASH_ZERO;
    return IDENT_TOKEN;
}

/**
 * It reads the next character from the input stream, and then checks if it's a punctuation, an
 * operator, a number, a char, a string, or an ID. If it's any of those, it returns the corresponding
 * token. If it's none of those, it returns an error token
 * 
 * @return The next token in the file.
 */
int consume_token(){
    int nextChar = skip_skippable();

    // If ERROR_TOKEN
    if(nextChar == ERROR_TOKEN){ return ERROR_TOKEN; }

    // If EOF
    if(nextChar == EOF_TOKEN){ return EOF_TOKEN; }

    //check the punctuation
    if(nextChar == OPEN_PARENTHESIS){ return OPEN_PAR_TOKEN; }
    if(nextChar == CLOSE_PARENTHESIS){ return CLOSE_PAR_TOKEN; }
    if(nextChar == OPEN_BRACKET){ return OPEN_BRACK_TOKEN; }
    if(nextChar == CLOSE_BRACKET){ return CLOSE_BRACK_TOKEN; }
    if(nextChar == OPEN_SQUARE_BRACKET){ return OPEN_SQUARE_BRACK_TOKEN; }
    if(nextChar == CLOSE_SQUARE_BRACKET){ return CLOSE_SQUARE_BRACK_TOKEN; }
    if(nextChar == SEMICOLON){ return SEMICOLON_TOKEN; }
    if(nextChar == POINT){ return DOT_TOKEN; }

    //Opertors
    if(nextChar == PLUS){ return ADD_TOKEN; }
    if(nextChar == MINUS){ return SUB_TOKEN; }
    if(nextChar == SLASH){ return DIV_TOKEN; }
    if(nextChar == MODULO){ return MOD_TOKEN; }
    if(nextChar == STAR){ return STAR_TOKEN; }
    if(nextChar == AND){ 
        //if &&
        if(lookNextChar() == AND){
            getNextChar(); //consume 
            return AND_TOKEN;
        }
        // if just &
        return ADR_TOKEN; 
    }
    if(nextChar == NOT){
        //if !=
        if(lookNextChar() == EQUALS){
            getNextChar();//consume 
            return NEQ_TOKEN;
        }
        return NOT_TOKEN; 
    }
    if(nextChar == OR){
        //if ||
        if(lookNextChar() == OR){
            getNextChar();//consume 
            return OR_TOKEN;
        }
        //else error 
        return ERROR_TOKEN; 
    }
    if(nextChar == EQUALS){
        //if compar ==
        if(lookNextChar() == EQUALS){
            getNextChar(); //consume 
            return EQ_TOKEN;
        }
        //else assign
        return ASSIGN_TOKEN;
    }
    if(nextChar == LESSTHAN){
        //if compar <=
        if(lookNextChar() == EQUALS){
            getNextChar(); //consume 
            return LESS_EQ_TOKEN;
        }
        //else <
        return LESS_TOKEN;
    }
    if(nextChar == MORETHAN){
        //if compar >=
        if(lookNextChar() == EQUALS){
            getNextChar(); //consume 
            return MORE_EQ_TOKEN;
        }
        //else >
        return MORE_TOKEN;
    }

    //Number Literal
    if(nextChar >= '0' && nextChar <= '9'){// if it's an alphanumeric
        get_number(nextChar);
        return INT_LITERAL_TOKEN;
    }
    //char Literal
    if(nextChar == SIMPLE_QUOTE) { 
        return get_char(nextChar);
    }
    //string Literal
    if(nextChar == DOUBLE_QUOTE) { 
        return get_string(nextChar);
    }

    // ID / Keyword / others
    return reconize_ID(nextChar);
}

// Store the potential unconsumed token, -1 otherwise
int tmp_token = -1;
/**
 * If there is no unconsumed token, get the next token. Otherwise, return the unconsumed token
 * 
 * @return The next token that has not been consumed.
 */
int get_next_unconsumed_token(){
    if(tmp_token == -1){ // if there is no unconsumed next : get the next
        tmp_token = consume_token();
        return tmp_token;
    }else{ //else return the curent unconsume
        return tmp_token;
    }
}
/**
 * It returns the next token, but if there's a token in the temporary buffer, it returns that instead
 * 
 * @return The next token.
 */
int get_next_token(){
    int res;
    if(tmp_token != -1){ // if exist a waiting token, choose this one
        res = tmp_token;
        tmp_token = -1;
    }else{// else chose the real next
        res = consume_token();
    }
    return res;
}

/*========================================================*/

/*========================== SYMBOLES TABLES =======================*/

/*===== Errors =====*/
#define ERROR_SYMBOLE_DEFINE_FULL       100 /* TOO MUCH DEFINE ERROR */
#define ERROR_SYMBOLE_VAR_FULL          101 /* TOO MUCH VARIABLE ERROR */
#define ERROR_IDENTIFIER_EXPECTED       102 /* MISSING IDENTIFIER */
#define ERROR_INT_LITERAL_EXPECTED      103 /* MISSING INT LITERAL */
#define ERROR_IMMEDIATE_TABLE_FULL      104 /* TOO MUCH IMMEDIATE VALUE ERROR */
#define ERROR_IMMEDIATE_WRONG_INDEX     105 /* WRONG INDEX IN IMMEDIATE TABLE */
#define ERROR_IMMEDIATE_WRONG_ACCESS    106 /* WRONG ACCESS IN IMMEDIATE TABLE */
#define ERROR_MISSING_VAR               107 /* MISSING VARIABLE IN VAR TABLE */
#define ERROR_ASSIGNIMENT_EXPECTED      108 /* MISSING ASSIGNEMENT TOKEN */
#define ERROR_SEMICOLON_EXPECTED        109 /* MISSING SEMICOLON */
#define ERROR_OPEN_PARENTESIS_EXPECTED  110 /* MISSING OPEN PARENTHESIS */
#define ERROR_CLOSE_PARENTESIS_EXPECTED 111 /* MISSING CLOSE PARENTESIS */
#define ERROR_WRONG_COMPARISON          112 /* WRONG COMPARISON TOKEN */
#define ERROR_WHILE_TOKEN_DO_EXPECTED   113 /* MISSING WHILE IN DO WHILE LOOP */
#define ERROR_OPEN_BRACKET_EXPECTED     114 /* MISSING OPEN BRACKET */

/**
 * It prints the error message and exits the program
 * 
 * @param errorType The type of error that occurred.
 */
void errorHandler(int errorType){
    switch (errorType)
    {
    case ERROR_SYMBOLE_DEFINE_FULL: 
        printf("[FATAL ERROR] at line %d : DEFINE SYMBOLE TABLE IS FULL !\n",fileLineCount);
        break;
    
    case ERROR_SYMBOLE_VAR_FULL:
        printf("[FATAL ERROR] at line %d : VAR SYMBOLE TABLE IS FULL !\n",fileLineCount);
        break;

    case ERROR_IDENTIFIER_EXPECTED:
        printf("[FATAL ERROR] at line %d : MISSING IDENTIFIER !\n",fileLineCount);
        break;

    case ERROR_INT_LITERAL_EXPECTED:
        printf("[FATAL ERROR] at line %d : MISSING INTEGER LITERAL !\n",fileLineCount);
        break;

    case ERROR_IMMEDIATE_TABLE_FULL:
        printf("[FATAL ERROR] at line %d : IMMEDIATE TABLE FULL !\n",fileLineCount);
        break;

    case ERROR_IMMEDIATE_WRONG_INDEX:
        printf("[FATAL ERROR] at line %d : IMMEDIATE INDEX IS NOT VALID !\n",fileLineCount);
        break;
        
    case ERROR_IMMEDIATE_WRONG_ACCESS:
        printf("[FATAL ERROR] at line %d : IMMEDIATE INDEX DOES NOT POINT TO A VALID VALUE !\n",fileLineCount);
        break;

    case ERROR_MISSING_VAR:
        printf("[FATAL ERROR] at line %d : MISSING VARIABLE IN VAR TABLE !\n",fileLineCount);
        break;

    case ERROR_ASSIGNIMENT_EXPECTED:
        printf("[FATAL ERROR] at line %d : MISSING ASSIGNEMENT ! ASSIGNEMENT EXPECTED !\n",fileLineCount);
        break;
        
    case ERROR_SEMICOLON_EXPECTED:
        printf("[FATAL ERROR] at line %d : MISSING SEMICOLON TOKEN !\n",fileLineCount);
        break;

    case ERROR_OPEN_PARENTESIS_EXPECTED:
        printf("[FATAL ERROR] at line %d : MISSING OPEN PARENTHESIS TOKEN !\n",fileLineCount);
        break;

    case ERROR_CLOSE_PARENTESIS_EXPECTED:
        printf("[FATAL ERROR] at line %d : MISSING CLOSE PARENTHESIS TOKEN !\n",fileLineCount);
        break;

    case ERROR_WRONG_COMPARISON:
        printf("[FATAL ERROR] at line %d : COMPARISON TOKEN UNKNOWN !\n",fileLineCount);
        break;

    case ERROR_WHILE_TOKEN_DO_EXPECTED:
        printf("[FATAL ERROR] at line %d : MISSING WHILE IN DO WHILE LOOP !\n",fileLineCount);
        break;
        
    case ERROR_OPEN_BRACKET_EXPECTED:
        printf("[FATAL ERROR] at line %d : MISSING OPEN BRACKET !\n",fileLineCount);
        break;

    default:
        printf("[FATAL ERROR] at line %d : UNKOWN ERROR !\n",fileLineCount);
        break;
    }
    printf("Test Failed");
    exit(EXIT_FAILURE);
}
/*==================*/

/*===== Define table =====*/
#define SYMBOLE_TABLE_DEFINE_SIZE 32
char symboleTableDefineName[SYMBOLE_TABLE_DEFINE_SIZE][TOKEN_VALUE_SIZE]; // a name can't be longer than the token value size
int symboleTableDefineValue[SYMBOLE_TABLE_DEFINE_SIZE];

/**
 * It initializes the define table by setting all the first characters of the names to the null
 * character
 */
void initDefineTable(){
    // we choose to set as empty the line if the first name char is 0
    // so for init, we set all the chars
    for(int i=0;i<SYMBOLE_TABLE_DEFINE_SIZE;i++){
        symboleTableDefineName[i][0] = BACK_SLASH_ZERO;
        symboleTableDefineValue[i] = -1;
    }
}

/**
 * It compares the token value with the name of the define in the symbol table
 * 
 * @param i the line number of the symbol table
 * 
 * @return true if the line i contains the token value, false otherwise.
 */
int stringCompareSymbTableANDtokVal(int i){
    if(symboleTableDefineName[i][0] != BACK_SLASH_ZERO){ // if it's equals to zero, the line is empty
        int j=0;
        while(symboleTableDefineName[i][j] == tokenCharValue[j]){
            if(symboleTableDefineName[i][j] == BACK_SLASH_ZERO){ return 1; }//both equals until the \0
            j++;
        }
    }
    return 0;//false, the line is not available OR there is no match between chains
}

/**
 * It searches the define table for a match between the token value and the value in the define table
 * 
 * @return The index of the symbol table where the token is found, -1 otherwise.
 */
int searchInDefineTable(){
    for(int i=0;i<SYMBOLE_TABLE_DEFINE_SIZE;i++){
        if(stringCompareSymbTableANDtokVal(i)){
            return i; // the symbole is on the i line
        }
    }
    return -1; // no symbole find
}

// NEW LINE AND EXISTING LINE IS DISTINGUABLE ?
/**
 * It adds a new define to the define table
 * 
 * @return The line of the added char
 */
int addInDefineTable(){
    int line = searchInDefineTable();
    if(line == -1){ // if the table doesn't already have the symbole
        for(int i=0;i<SYMBOLE_TABLE_DEFINE_SIZE;i++){ // search a free line among all
            if(symboleTableDefineName[i][0] == BACK_SLASH_ZERO){  // if the line is free
                int j = 0;

                while(tokenCharValue[j] != BACK_SLASH_ZERO){// add the chain
                    // no overflow thanks to the equals size between the names 
                    symboleTableDefineName[i][j] = tokenCharValue[j];

                    // index incrementation
                    j++;
                }
                symboleTableDefineName[i][j] = BACK_SLASH_ZERO;
                return i; // return the line of the added char
            }
        }
        //ERROR CASE ! NOT ENOUGH PLACES
        errorHandler(ERROR_SYMBOLE_DEFINE_FULL);
    }
    return line;
}

/**
 * This function adds the token value to the corresponding line in the define table.
 * 
 * @param line the line number of the define statement
 * @param value the value of the token
 */
void addValueInDefineTable(int line,int value){
    // add the token value to the corresponding line            
    symboleTableDefineValue[line] = value;
}

/**
 * @brief FOR DEBUG PURPOSES ONLY !!
 * @todo DELETE
 *
 * It prints the define table.
 */
void printDefineTable(){
    for(int i=0;i<SYMBOLE_TABLE_DEFINE_SIZE;i++){
        printf("DEFINE TAB[%d] = %s ; value = %d\n",i,symboleTableDefineName[i],symboleTableDefineValue[i]);
    }
}
/*==================*/

/*===== Variable table =====*/

#define SYMBOLE_TABLE_VAR_SIZE 32
char symboleTableVarName[SYMBOLE_TABLE_VAR_SIZE][TOKEN_VALUE_SIZE]; // a name can't be longer than the token value size
int symboleTableVarValue[SYMBOLE_TABLE_VAR_SIZE];

/**
 * It initializes the variable table by setting all the variable names to the empty string and all the
 * variable values to 0
 */
void initVarTable(){
    // we choose to set as empty the line if the first name char is 0
    // so for init, we set all the chars
    for(int i=0;i<SYMBOLE_TABLE_VAR_SIZE;i++){
        symboleTableVarName[i][0] = BACK_SLASH_ZERO;
        symboleTableVarValue[i] = 0;
    }
}

/**
 * It compares the tokenCharValue with the symboleTableVarName[i] and returns 1 if they are equal, 0
 * otherwise
 * 
 * @param i the line number in the table
 * 
 * @return the index of the line in the table where the variable is stored.
 */
int stringCompareSymbVarTable(int i){
    if(symboleTableVarName[i][0] != BACK_SLASH_ZERO){ // if it's equals to zero, the line is empty
        int j=0;
        while(symboleTableVarName[i][j] == tokenCharValue[j]){
            if(symboleTableVarName[i][j] == BACK_SLASH_ZERO){ return 1; }//both equals until the \0
            j++;
        }
    }
    return 0;//false, the line is not available OR there is no match between chains
}

/**
 * It searches for a variable in the variable table
 * 
 * @return The index of the symbol in the symbol table.
 */
int searchInVarTable(){
    for(int i=0;i<SYMBOLE_TABLE_VAR_SIZE;i++){ //for each entry on symb var table
        if(stringCompareSymbVarTable(i)){//check if the both names corresponds
            return i; // the symbole is on the i line
        }
    }
    return -1; // no symbole find
}

/**
 * It adds a variable in the variable table if it's not already in it
 * 
 * @return The line of the added char
 */
int addInVarTable(){
    int line = searchInVarTable();
    if(line == -1){ // if the table doesn't already have the symbole
        for(int i=0;i<SYMBOLE_TABLE_VAR_SIZE;i++){ // search a free line among all
            if(symboleTableVarName[i][0] == BACK_SLASH_ZERO){  // if the line is free
                int j = 0;

                while(tokenCharValue[j] != BACK_SLASH_ZERO){// add the chain
                    // no overflow thanks to the equals size between the names 
                    symboleTableVarName[i][j] = tokenCharValue[j];

                    // index incrementation
                    j++;
                }
                symboleTableVarName[i][j] = BACK_SLASH_ZERO;
                return i; // return the line of the added char
            }
        }
        //ERROR CASE ! NOT ENOUGH PLACES
        errorHandler(ERROR_SYMBOLE_VAR_FULL);
    }
    return line;
}

/**
 * It adds the value of a variable to the symbol table
 * 
 * @param line the line number of the variable in the source code
 * @param value the value of the token
 */
void addValueInVarTable(int line,int value){
    // add the token value to the corresponding line
    symboleTableVarValue[line] = value;
}

/**
 * It prints the variable table
 */
void printVariableTable(){
    for(int i=0;i<SYMBOLE_TABLE_VAR_SIZE;i++){
        printf("VARIABLE TAB[%d] = %s ; value = %d\n",i,symboleTableVarName[i],symboleTableVarValue[i]);
    }
}


/*===== Immediate Expression Table =====*/

// The table of immediate value that will be stored
// in order to parse expressions
#define IMMEDIATE_TABLE_VAR_SIZE 32
int immediateTable[IMMEDIATE_TABLE_VAR_SIZE];
int immediateTableElementNumber; //actual number of immediate values

/**
 * It initializes the immediate table.
 */
void initiateImmediateTable(){
    // We put zero because we have the clear in the accumulator
    // So it is the only variable that is not needed
    immediateTableElementNumber = 0;
    for(int i = 0;i<IMMEDIATE_TABLE_VAR_SIZE;i++){
        immediateTable[i] = 0;
    }
}

/**
 * It checks if the value is in the immediate table, and if it is, it returns the index of the value in
 * the table
 * 
 * @param value the value to be searched in the table
 * 
 * @return The index of the value in the table.
 */
int findValueInImmediateTable(int value){
    //for each immediate value in the table, check is the both values are equals
    for(int i = 0 ;i<IMMEDIATE_TABLE_VAR_SIZE;i++){
        if(value == immediateTable[i]){return i;}
    }
    return -1;
}

/**
 * It adds a value to the immediate table if it doesn't already exist, and returns the index of the
 * value in the table
 * 
 * @param value the value to add to the table
 * 
 * @return The index of the value in the table
 */
int addValueToImmediateTable(int value){
    int a = findValueInImmediateTable(value); // check if the value already exist
    if(a == -1 && immediateTableElementNumber <= IMMEDIATE_TABLE_VAR_SIZE){// if they don't exists, and there is enough places 
        //printf("Ajout de la val %d a la pos %d\n",value,immediateTableElementNumber);
        immediateTable[immediateTableElementNumber] = value; // add the value
        immediateTableElementNumber++;  // update the number
        return immediateTableElementNumber-1; // return the line
    }
    else{
        // Aleady in the table
        // Then we return the index
        return a;
    }

    // The table is full
    errorHandler(ERROR_IMMEDIATE_TABLE_FULL);
    return -1;

}

/**
 * It returns the value of the immediate table at the given index
 * 
 * @param index the index of the variable in the immediate table
 * 
 * @return The value of the immediate table at the given index.
 */
int getValueFromImmediateTable(int index){
    // if the input is bad
    if(index < 0 || index >= IMMEDIATE_TABLE_VAR_SIZE){ errorHandler(ERROR_IMMEDIATE_WRONG_INDEX); return -1; }

    int a = immediateTable[index];
    if(a == 0){// if the value is undefined TODO, ca marche pas si on met un ZERO ?
        errorHandler(ERROR_IMMEDIATE_WRONG_ACCESS);
        return -1;
    }
    else{
        return a;
    }
}

/**
 * It prints the immediate table
 */
void printImmediateTable(){
    for(int i=0;i<IMMEDIATE_TABLE_VAR_SIZE;i++){
        printf("IMMEDIATE_TABLE[%d] = %d\n",i,immediateTable[i]);
    }
}

/*========================================================*/


/*========================== Generating code methods =======================*/

// Buffer of the name to print
char nameToPrint [TOKEN_VALUE_SIZE];

/**
 * It sets the PC to 0.
 */
void initCodeGeneration(){
    fprintf(outfile,"    org 0\n");//set PC
}

/**
 * It generates the code for printing the result of the expression
 */
void generatePrintCode(){
    fprintf(outfile,"    out *\n"); // print code
}

/**
 * It prints out the assembly code to load the value at the address of the variable nameToPrint
 */
void generateLoadCode(){
    fprintf(outfile,"    lda %s\n",nameToPrint); // load the value at the nameToPrint address
}

/**
 * It generates the code to store the value of the expression on the top of the stack into the variable
 * whose name is stored in the global variable nameToPrint
 */
void generateStoreCode(){
    fprintf(outfile,"    sta %s\n",nameToPrint); //store the value on nameToPrint address
}

/**
 * It prints the assembly code for adding the value on the address in nameToPrint to the akku
 */
void generateAddCode(){ 
    fprintf(outfile,"    add %s\n",nameToPrint); // add the value on nameToPrint address to the akku
}

/**
 * It generates the code for the substraction of two variables
 */
void generateSubCode(){
    // we use the substract by adding a 2's complement method
    fprintf(outfile,"    sta tmpVarArithm\n");//tmp = akku
    fprintf(outfile,"    lda %s\n",nameToPrint);
    fprintf(outfile,"    nor zero\n");
    fprintf(outfile,"    add one\n"); // akku = -nameToPrint
    fprintf(outfile,"    add tmpVarArithm\n"); //akku = -nameToPrint + tmp
}

//count the number of mul in the program to get accurates tags
int nbMul = 0;
/**
 * It generates the code for the multiplication of two numbers
 */
void generateMulCode(){ // TODO CAS 0
    fprintf(outfile,"    sta tmpVarArithm\n");// tmpVarArithm contain the remaining number of add
    // tmpVarArithm2 will contain the result
    fprintf(outfile,"    lda zero\n");
    fprintf(outfile,"    sta tmpVarArithm2\n");
    
    // SUB 1 to tmpVarArithm (cause of the condition >= 0 of the algorithme)
    fprintf(outfile,"    lda allone\n");
    fprintf(outfile,"    add tmpVarArithm\n");

    fprintf(outfile,"mul%d sta tmpVarArithm\n",nbMul); // markup of multiplication && tmp = akku
    fprintf(outfile,"    lda tmpVarArithm2\n");// load the current Res
    fprintf(outfile,"    add %s\n",nameToPrint);// add
    fprintf(outfile,"    sta tmpVarArithm2\n");// store the current Res

    //sub 1 to the number of times
    fprintf(outfile,"    lda allone\n");
    fprintf(outfile,"    add tmpVarArithm\n");

    //if the value of the carry becomes 1 (value == 0) then end
    fprintf(outfile,"    jcs mul%d\n",nbMul);
    fprintf(outfile,"    lda tmpVarArithm2\n"); // akku = res
    nbMul++;
}

//count the number of div in the program to get accurates tags (as mul)
int nbDiv = 0;
/**
 * It generates the code for the division
 */
void generateDivCode(){ //todo faire division par 0
    fprintf(outfile,"    sta tmpVarArithm\n");// tmpVarArithm contain the current res of successives subs
    // tmpVarArithm2 contain the result (nb of effective subs)
    fprintf(outfile,"    lda zero\n");
    fprintf(outfile,"    sta tmpVarArithm2\n");

    fprintf(outfile,"    lda tmpVarArithm\n");
    fprintf(outfile,"div%d sta tmpVarArithm\n",nbDiv);
    
    //incr1 tmp2 (the result)
    fprintf(outfile,"    lda tmpVarArithm2\n");
    fprintf(outfile,"    add one\n");
    fprintf(outfile,"    sta tmpVarArithm2\n");

    // SUB
    fprintf(outfile,"    lda %s\n",nameToPrint);
    fprintf(outfile,"    nor zero\n");
    fprintf(outfile,"    add one\n");
    fprintf(outfile,"    add tmpVarArithm\n");
    // END SUB

    //if the value of the carry becomes 1 (value == 0) then end
    fprintf(outfile,"    jcs div%d\n",nbDiv);
    //res = res-1 due to the >= condition
    fprintf(outfile,"    lda allone\n");
    fprintf(outfile,"    add tmpVarArithm2\n");
    nbDiv++;
}

/**
 * It stores the value of the accumulator in a temporary variable, loads the value of -1 into the
 * accumulator, and adds the value of the temporary variable to the accumulator
 */
void generateSubOneCode(){
    fprintf(outfile,"    sta tmpVarArithm\n");//tmp = akku
    fprintf(outfile,"    lda allone\n");
    fprintf(outfile,"    add tmpVarArithm\n"); //akku = -1 + tmp
}

/**
 * It generates the code for the subtraction of the two values to compare
 */
void generateIfSubCode(){
    //sub of the to value : _ifTmp1 - _ifTmp2 to get the carry (for the conditionnal jump)
    fprintf(outfile,"    lda _ifTmp2\n"); 
    fprintf(outfile,"    nor zero\n");
    fprintf(outfile,"    add one\n");
    fprintf(outfile,"    add _ifTmp1\n");
}

void generateIFSubBACode(){
    //sub of the to value : _ifTmp2 - _ifTmp1 to get the carry (for the conditionnal jump)
    fprintf(outfile,"    lda _ifTmp1\n"); 
    fprintf(outfile,"    nor zero\n");
    fprintf(outfile,"    add one\n");
    fprintf(outfile,"    add _ifTmp2\n");
}

/**
 * It generates the code for a jump to the end of the if statement
 * 
 * @param nbIf the number of the if statement
 */
void generateJumpEndIfCode(int nbIf){
    fprintf(outfile,"    jcs endif%d\n",nbIf);// create a jump to the endif tag //TODO si nb>10
}

void generateJumpStartIfCode(int nbIf){
    fprintf(outfile,"    jcs startif%d\n",nbIf);// create a jump to the endif tag//TODO si nb>10
}

void generateJumpStartElseCode(int nbIf){
    fprintf(outfile,"    jcs startelse%d\n",nbIf);// create a jump to the endif tag//TODO si nb>10
}

/**
 * It generates the endif tag for the if statement
 * 
 * @param nbIf the number of the if statement
 */
void generateJumpEndIfTag(int nbIf){
    fprintf(outfile,"endif%d lda zero\n",nbIf);// create the endif tag//TODO si nb>10
}

void generateJumpStartIfTag(int nbIf){
    fprintf(outfile,"startif%d lda zero\n",nbIf);// create the endif tag//TODO si nb>10
}

void generateJumpStartElseTag(int nbIf){
    fprintf(outfile,"   jmp endif%d\n",nbIf);
    fprintf(outfile,"startelse%d lda zero\n",nbIf);// create the endif tag//TODO si nb>10
}

void generateNEQCondition(int nbIf,int numCond){ // a in tmp1 and b in tmp2
    // do the sub a-b < 0 check a<b
    generateIfSubCode();
    // if C == 1 <=> a-b>= 0, so we have to test the second term 
    fprintf(outfile,"    jcs _evalIfConditionNEQ%d_%d\n",nbIf,numCond);
    fprintf(outfile,"    jmp startif%d_%d\n",nbIf,numCond);// if C == 0, jump startIf
    
    fprintf(outfile,"_evalIfConditionNEQ%d_%d lda zero\n",nbIf,numCond);// check the second condition
    generateIFSubBACode();//compute b-a : C == 1 <=> b-a >= 0
    fprintf(outfile,"    jcs startelse%d_%d\n",nbIf,numCond);
    fprintf(outfile,"    jmp startif%d_%d\n",nbIf,numCond);// if C == 0, jump startIf
}

void generateEQCondition(int nbIf,int numCond){ // a in tmp1 and b in tmp2
    // do the sub a-b < 0 check a<b
    generateIfSubCode();
    // if C == 1 <=> a-b>= 0, so we have to test the second term 
    fprintf(outfile,"    jcs _evalIfConditionEQ%d_%d\n",nbIf,numCond);
    fprintf(outfile,"    jmp startelse%d_%d\n",nbIf,numCond);// if C == 0, jump startIf
    
    fprintf(outfile,"_evalIfConditionEQ%d_%d lda zero\n",nbIf,numCond);// check the second condition
    generateIFSubBACode();//compute b-a : C == 1 <=> b-a >= 0
    fprintf(outfile,"    jcs startif%d_%d\n",nbIf,numCond);
    fprintf(outfile,"    jmp startelse%d_%d\n",nbIf,numCond);// if C == 0, jump startIf
}

void generateLessCondition(int nbIf,int numCond){ // a in tmp1 and b in tmp2
    // do the sub a-b < 0 check a<b
    generateIfSubCode();
    // if C == 1 <=> a-b>= 0, so we have to test the second term 
    fprintf(outfile,"    jcs startelse%d_%d\n",nbIf,numCond);
    fprintf(outfile,"    jmp startif%d_%d\n",nbIf,numCond);// if C == 0, jump startIf
}

void generateMoreCondition(int nbIf,int numCond){ // a in tmp1 and b in tmp2
    // do the sub b-a < 0 : C == 1 <=> b-a >= 0
    generateIFSubBACode();
    // if C == 1 <=> b-a >= 0, so we have to test the second term 
    fprintf(outfile,"    jcs startelse%d_%d\n",nbIf,numCond);
    fprintf(outfile,"    jmp startif%d_%d\n",nbIf,numCond);// if C == 0, jump startIf
}

void generateMoreEqualCondition(int nbIf,int numCond){ // a in tmp1 and b in tmp2
    // do the sub b-a < 0 : C == 1 <=> b-a >= 0
    generateIfSubCode();
    // if C == 1 <=> b-a >= 0, so we have to test the second term 
    fprintf(outfile,"    jcs startif%d_%d\n",nbIf,numCond);
    fprintf(outfile,"    jmp startelse%d_%d\n",nbIf,numCond);// if C == 0, jump startIf
}

void generateLessEqualCondition(int nbIf,int numCond){ // a in tmp1 and b in tmp2
    // do the sub b-a < 0 : C == 1 <=> b-a >= 0
    generateIFSubBACode();
    // if C == 1 <=> b-a >= 0, so we have to test the second term 
    fprintf(outfile,"    jcs startif%d_%d\n",nbIf,numCond);
    fprintf(outfile,"    jmp startelse%d_%d\n",nbIf,numCond);// if C == 0, jump startIf
}

void generateCondBoolNot(int nbIf,int numCond){
    fprintf(outfile,"startif%d_%d jmp startelse%d_%d\n",nbIf,numCond,nbIf,numCond+1);
    fprintf(outfile,"startelse%d_%d jmp startif%d_%d\n",nbIf,numCond,nbIf,numCond+1);
}

void generateCondBoolAnd(int nbIf,int numCond){
    fprintf(outfile,"startelse%d_%d jmp startelse%d_%d\n",nbIf,numCond-1,nbIf,numCond);
    
    fprintf(outfile,"startif%d_%d lda zero\n",nbIf,numCond-1);
}

void generateCondBoolOr(int nbIf,int numCond){
    fprintf(outfile,"startif%d_%d jmp startif%d_%d\n",nbIf,numCond-1,nbIf,numCond);
    fprintf(outfile,"startelse%d_%d jmp startif%d_%d\n",nbIf,numCond-1,nbIf,numCond);
}

void generateCondBoolToCond(int nbIf,int numCond){
    fprintf(outfile,"startif%d_%d jmp startif%d\n",nbIf,numCond,nbIf);
    fprintf(outfile,"startelse%d_%d jmp startelse%d\n",nbIf,numCond,nbIf);
}


//TODO may be fuse with the if
void generateJumpStartWhile(int nbWhile){
    fprintf(outfile,"    jmp startWhile%d_D\n",nbWhile);// create a jump to the endif tag//TODO si nb>10
    fprintf(outfile,"endWhile%d lda zero\n",nbWhile);
}

void generateTagStartWhile(int nbWhile){
    fprintf(outfile,"startWhile%d_D lda zero\n",nbWhile);// create a jump to the endif tag//TODO si nb>10
}


void generateWhileSubCode(){
    //sub of the to value : _WhileTmp1 - _WhileTmp2 to get the carry (for the conditionnal jump)
    fprintf(outfile,"    lda _WhileTmp2\n"); 
    fprintf(outfile,"    nor zero\n");
    fprintf(outfile,"    add one\n");
    fprintf(outfile,"    add _WhileTmp1\n");
}

void generateWhileSubBACode(){
    //sub of the to value : _WhileTmp2 - _WhileTmp1 to get the carry (for the conditionnal jump)
    fprintf(outfile,"    lda _WhileTmp1\n"); 
    fprintf(outfile,"    nor zero\n");
    fprintf(outfile,"    add one\n");
    fprintf(outfile,"    add _WhileTmp2\n");
}

void generateJumpEndWhileCode(int nbWhile){
    fprintf(outfile,"    jcs endWhile%d\n",nbWhile);// create a jump to the endif tag //TODO si nb>10
}

void generateJumpStartWhileCode(int nbWhile){
    fprintf(outfile,"    jcs startWhile%d\n",nbWhile);// create a jump to the endif tag//TODO si nb>10
}

void generateJumpEndWhileTag(int nbWhile){
    fprintf(outfile,"endWhile%d lda zero\n",nbWhile);// create the endif tag//TODO si nb>10
}

void generateJumpStartWhileTag(int nbWhile){
    fprintf(outfile,"startWhile%d lda zero\n",nbWhile);// create the endif tag//TODO si nb>10
}

void generateWhileNEQCondition(int nbWhile,int numCond){ // a in tmp1 and b in tmp2
    // do the sub a-b < 0 check a<b
    generateWhileSubCode();
    // if C == 1 <=> a-b>= 0, so we have to test the second term 
    fprintf(outfile,"    jcs _evalWhileConditionNEQ%d_%d\n",nbWhile,numCond);
    fprintf(outfile,"    jmp startWhile%d_%d\n",nbWhile,numCond);// if C == 0, jump startIf
    
    fprintf(outfile,"_evalWhileConditionNEQ%d_%d lda zero\n",nbWhile,numCond);// check the second condition
    generateIFSubBACode();//compute b-a : C == 1 <=> b-a >= 0
    fprintf(outfile,"    jcs endWhile%d_%d\n",nbWhile,numCond);
    fprintf(outfile,"    jmp startWhile%d_%d\n",nbWhile,numCond);// if C == 0, jump startIf
}

void generateWhileEQCondition(int nbWhile,int numCond){ // a in tmp1 and b in tmp2
    // do the sub a-b < 0 check a<b
    generateWhileSubCode();
    // if C == 1 <=> a-b>= 0, so we have to test the second term 
    fprintf(outfile,"    jcs _evalWhileConditionEQ%d_%d\n",nbWhile,numCond);
    fprintf(outfile,"    jmp endWhile%d_%d\n",nbWhile,numCond);// if C == 0, jump startIf
    
    fprintf(outfile,"_evalWhileConditionEQ%d_%d lda zero\n",nbWhile,numCond);// check the second condition
    generateIFSubBACode();//compute b-a : C == 1 <=> b-a >= 0
    fprintf(outfile,"    jcs startWhile%d_%d\n",nbWhile,numCond);
    fprintf(outfile,"    jmp endWhile%d_%d\n",nbWhile,numCond);// if C == 0, jump startIf
}

void generateWhileLessCondition(int nbWhile,int numCond){ // a in tmp1 and b in tmp2
    // do the sub a-b < 0 check a<b
    generateWhileSubCode();
    // if C == 1 <=> a-b>= 0, so we have to test the second term 
    fprintf(outfile,"    jcs endWhile%d_%d\n",nbWhile,numCond);
    fprintf(outfile,"    jmp startWhile%d_%d\n",nbWhile,numCond);// if C == 0, jump startIf
}

void generateWhileMoreCondition(int nbWhile,int numCond){ // a in tmp1 and b in tmp2
    // do the sub b-a < 0 : C == 1 <=> b-a >= 0
    generateWhileSubBACode();
    // if C == 1 <=> b-a >= 0, so we have to test the second term 
    fprintf(outfile,"    jcs endWhile%d_%d\n",nbWhile,numCond);
    fprintf(outfile,"    jmp startWhile%d_%d\n",nbWhile,numCond);// if C == 0, jump startIf
}

void generateWhileMoreEqualCondition(int nbWhile,int numCond){ // a in tmp1 and b in tmp2
    // do the sub b-a < 0 : C == 1 <=> b-a >= 0
    generateWhileSubCode();
    // if C == 1 <=> b-a >= 0, so we have to test the second term 
    fprintf(outfile,"    jcs startWhile%d_%d\n",nbWhile,numCond);
    fprintf(outfile,"    jmp endWhile%d_%d\n",nbWhile,numCond);// if C == 0, jump startIf
}

void generateWhileLessEqualCondition(int nbWhile,int numCond){ // a in tmp1 and b in tmp2
    // do the sub b-a < 0 : C == 1 <=> b-a >= 0
    generateWhileSubBACode();
    // if C == 1 <=> b-a >= 0, so we have to test the second term 
    fprintf(outfile,"    jcs startWhile%d_%d\n",nbWhile,numCond);
    fprintf(outfile,"    jmp endWhile%d_%d\n",nbWhile,numCond);// if C == 0, jump startIf
}

void generateWhileCondBoolNot(int nbIf,int numCond){
    fprintf(outfile,"startWhile%d_%d jmp endWhile%d_%d\n",nbIf,numCond,nbIf,numCond+1);
    fprintf(outfile,"endWhile%d_%d jmp startWhile%d_%d\n",nbIf,numCond,nbIf,numCond+1);
}

void generateWhileCondBoolAnd(int nbIf,int numCond){
    fprintf(outfile,"endWhile%d_%d jmp endWhile%d_%d\n",nbIf,numCond-1,nbIf,numCond);
    fprintf(outfile,"startWhile%d_%d jmp startWhile%d_%d\n",nbIf,numCond-1,nbIf,numCond);
}

void generateWhileCondBoolOr(int nbIf,int numCond){
    fprintf(outfile,"startWhile%d_%d jmp startWhile%d_%d\n",nbIf,numCond-1,nbIf,numCond+1);
    fprintf(outfile,"endWhile%d_%d jmp startWhile%d_%d\n",nbIf,numCond-1,nbIf,numCond);
}

void generateWhileCondBoolToCond(int nbIf,int numCond){
    fprintf(outfile,"startWhile%d_%d jmp startWhile%d\n",nbIf,numCond,nbIf);
    fprintf(outfile,"endWhile%d_%d jmp endWhile%d\n",nbIf,numCond,nbIf);
}



//code for expressions 
#define LoadExpression 0
#define AddExpression 1
#define SubExpression 2
#define MulExpression 3
#define DivExpression 4
int operationExpression = LoadExpression; // during the parsing of an expression, contain the current operation

/**
 * set the current expression
 * 
 * @param opExpression The operation expression to be used.
 */
void setOperationExpression( int opExpression ){
    operationExpression = opExpression;
}
/**
 * It generates the code for the current operation
 */
void generateExpressionCode(){
    switch (operationExpression)//in function of the current operation, apply the good code generator
    {
    case LoadExpression:
        generateLoadCode();
        break;
    case AddExpression:
        generateAddCode();
        break;
    case SubExpression:
        generateSubCode();
        break;
    case MulExpression:
        generateMulCode();
        break;
    case DivExpression:
        generateDivCode();
        break;
    default:
        // TODO, error case
        break;
    }
}

//code for expressions with some ()
#define maxImbrPrioExpressions 10
// save the op during the multi phases of the parsing expression with ()
int opModePrioExpressions[maxImbrPrioExpressions];
int nbPrioExprTot = 0; // number tot of the nested ()
int nbPrioCurrent = 0; // current level of nested
void savePrioExpression(){// todo faire imbrication 
    opModePrioExpressions[nbPrioCurrent] = operationExpression;
    nbPrioExprTot ++;
    nbPrioCurrent ++;
    fprintf(outfile,"    sta _prioExpr%d\n",nbPrioCurrent);
}
void restorePrioExpression(){
    fprintf(outfile,"    sta _prioExprRet%d\n",nbPrioCurrent);
    fprintf(outfile,"    lda _prioExpr%d\n",nbPrioCurrent);
    setOperationExpression(opModePrioExpressions[nbPrioCurrent -1]);
    //load in the printBuffer the name 
    nameToPrint[0] = '_';
    nameToPrint[1] = 'p'; //TODO faire mieux
    nameToPrint[2] = 'r';
    nameToPrint[3] = 'i';
    nameToPrint[4] = 'o';
    nameToPrint[5] = 'E';
    nameToPrint[6] = 'x';
    nameToPrint[7] = 'p';
    nameToPrint[8] = 'r';
    nameToPrint[9] = 'R';
    nameToPrint[10] = 'e';
    nameToPrint[11] = 't';
    nameToPrint[12] = nbPrioCurrent + 48; // ne marche pas tout le temps x) passage ASCII TODO
    nameToPrint[13] = '\n';
    nbPrioCurrent --;
}

/**
 * It takes a line number and puts it in the buffer to be printed
 * 
 * @param line the line number of the instruction
 */
void loadImmediateValue(int line){
    //load an immediate value in the printed buffer
    nameToPrint[0] = 'i';
    nameToPrint[1] = 'm';
    //TODO faire mieux
    nameToPrint[2] = ((line/100) % 10) + 48;
    nameToPrint[3] = ((line/10) % 10) + 48;
    nameToPrint[4] = ((line) % 10) + 48;
    nameToPrint[5] = '\0';
}

/**
 * It loads the name of the variable on line line of the symbole table to the print buffer
 * 
 * @param line the line of the variable in the symbole table
 */
void loadVariableValue(int line){
    int i=0;
    while(symboleTableVarName[line][i] != '\0'){ // load the name on line line to the print buffer
        nameToPrint[i] = symboleTableVarName[line][i];
        i++;
    }
    nameToPrint[i] = '\0';//dont forget the /0
}

/**
 * It loads the name of the temporary variable into the array nameToPrint
 * 
 * @param numTmp 1 or 2
 */
void loadIfTmp(int numTmp){ //numTmp == 1 OU 2
    nameToPrint[0] = '_';
    nameToPrint[1] = 'i';
    nameToPrint[2] = 'f';
    nameToPrint[3] = 'T';
    nameToPrint[4] = 'm';
    nameToPrint[5] = 'p';
    nameToPrint[6] = numTmp + 48;//for ascii value
    nameToPrint[7] = '\n';
}

/**
 * It loads the name of the temporary variable into the array nameToPrint
 * 
 * @param numTmp 1 or 2
 */
void loadWhileTmp(int numTmp){ //numTmp == 1 OU 2
    nameToPrint[0] = '_';
    nameToPrint[1] = 'W';
    nameToPrint[2] = 'h';
    nameToPrint[3] = 'i';
    nameToPrint[4] = 'l';
    nameToPrint[5] = 'e';
    nameToPrint[6] = 'T';
    nameToPrint[7] = 'm';
    nameToPrint[8] = 'p';
    nameToPrint[9] = numTmp + 48;//for ascii value
    nameToPrint[10] = '\n';
}

/**
 * It writes the symbole tables in the output file
 */
void writeSymboleTables(){//TODO prefixer variable par un _
    //Temp variable for special purposes
    fprintf(outfile,"\ntmpVarArithm dcb 0\n"); // VAR used for arirhmetic purposes
    fprintf(outfile,"tmpVarArithm2 dcb 0\n"); // VAR used for arirhmetic purposes
    fprintf(outfile,"tmpVarArithm3 dcb 0\n"); // VAR used for arirhmetic purposes
    fprintf(outfile,"_ifTmp1 dcb 0\n"); //for if conditions
    fprintf(outfile,"_ifTmp2 dcb 0\n"); //for if conditions
    fprintf(outfile,"_WhileTmp1 dcb 0\n"); //for if conditions
    fprintf(outfile,"_WhileTmp2 dcb 0\n"); //for if conditions
    //write the tmp buffer of the prio exressions
    for(int i=1;i<=nbPrioExprTot;i++){ //for prior expressions
        fprintf(outfile,"_prioExpr%d dcb 0\n",i);
        fprintf(outfile,"_prioExprRet%d dcb 0\n",i);
    }
    fprintf(outfile,"allone dcb 255\n"); // Var that represent 0xFF
    fprintf(outfile,"one dcb 1\n"); // Var that represent 0x01
    fprintf(outfile,"zero dcb 0\n"); // Var that represent 0x00    

    //write Imm table :
    for(int i=0;i<immediateTableElementNumber;i++){
        fprintf(outfile,"im%c%c%c dcb %d\n",((i/100) % 10) + 48,((i/10) % 10) + 48,(i % 10) + 48,immediateTable[i]);
    }

    //write variables table (only the define ones)
    for(int line = 0;line < SYMBOLE_TABLE_VAR_SIZE; line ++){
        if(symboleTableVarName[line][0] != '\0'){
            fprintf(outfile,"%s dcb 0\n",symboleTableVarName[line]);
        }
    }
}

/*========================================================*/

/*========================== PARSER DEFINITIONS & FUNCTIONS =======================*/


/**
 * The function parseDefine() parses a #DEFINE directive, and add the values in the global define tables 
 */
void parseDefine(){ // parse #DEFINE <name> <value>
    /* We need to get the next token (the name) */
    int tok = get_next_token();
    // if the reading token is not an identifiers, it's doesn't match with the grammar
    if(tok != IDENT_TOKEN){
        errorHandler(ERROR_IDENTIFIER_EXPECTED);
    }
    // name add of the identifiers in the current table (and check if it already exist)
    int line = addInDefineTable(); // line is the line number in the table
    
    // read the next token 
    tok = get_next_token(); 
    if(tok != INT_LITERAL_TOKEN){ // if it's not an integer, doesn't match with grammar
        errorHandler(ERROR_INT_LITERAL_EXPECTED);
    }
    addValueInDefineTable(line,tokenIntValue);

    //for debug purpose
    //printDefineTable();
}

/* === EXPRESSIONS === */
void parseExpressionE();
void parseExpressionT();

/**
 * It parses an expression of the form F, which is either an integer literal, an identifier or a
 * parenthesis expression
 * 
 * expressionF = (expressionE) | Int_literral | Identifiant
 */
void parseExpressionF(){
    int next = get_next_token();
    int line;
    switch (next)
    {
    case INT_LITERAL_TOKEN:// ex : 8
        // printf("INT LIT avec %d\n",tokenIntValue);// DEBUG line

        line = addValueToImmediateTable(tokenIntValue); //contain the current litteral value

        /* load the name in the current name buffer */
        loadImmediateValue(line); // load the imediate name in the printed output buffer
        break;
    case IDENT_TOKEN:// ex : a
        line = searchInVarTable(); // search the line corresponding to the current tokenStringValue
        if(line == -1) { // if no line found, raise an error, variable undefined
            errorHandler(ERROR_MISSING_VAR);
        }
        loadVariableValue(line); // load the variable name in the printed output buffer
        break;
    case OPEN_PAR_TOKEN://(3+b)
        // expression prio

        // save akku value + operation mode
        savePrioExpression();

        //parse the inside parenthesis expression
        parseExpressionE();

        //chek the good end
        next = get_next_token();
        if(next != CLOSE_PAR_TOKEN){
            //ERROR CASE !
        }

        //in akku, there is the expression value of the parenthesis code
        //save this value, restore akku to the previous value and restore operation mode
        restorePrioExpression();
        //no we must have to apply the operartion to the saved value

        break;
    default:
        //TODO ERROR case
        break;
    }
}

/**
 * `parseExpressionTPrime()` is a function that parses the grammar rule `<expressionTPrime>` which is a
 * non-terminal symbol in the grammar. 
 * 
 * The grammar rule is: expressionTPrime =  | '*' expressionF . expressionTPrime 
                                            | '/' expressionF . expressionTPrime 
                                            | epsilon
 */
void parseExpressionTPrime(){
    int token = get_next_unconsumed_token();
    switch (token)
    {
    case STAR_TOKEN:// mul case *
        //consume the token STAR_TOKEN
        get_next_token();
        
        // set the current operation to mul operation
        setOperationExpression(MulExpression);
        parseExpressionF();
        
        //we have to generate the code here before see another potential expression
        generateExpressionCode(); 
        parseExpressionTPrime();
        
        break;
    case DIV_TOKEN:// div case /
        //consume the token DIV_TOKEN
        get_next_token();

        // set the current operation to div operation
        setOperationExpression(DivExpression);
        parseExpressionF();
        
        //we have to generate the code here before see another potential expression
        generateExpressionCode();  
        parseExpressionTPrime();
        
        break;
    default:
        // EPSILON case
        break;
    }
}

/**
 * `parseExpressionT` parses an expression, and then parses an expression prime
 * 
 * expressionT = expressionF . expressionTPrime
 */
void parseExpressionT(){
    parseExpressionF();
    // generate the operation with current nameToPrint address
    generateExpressionCode(); 
    parseExpressionTPrime();
}

/**
 * If the next token is an ADD_TOKEN or SUB_TOKEN, then consume the token and call parseExpressionT()
 * and parseExpressionEPrime()
 * 
 * expressionEPrime =   | '+' expressionT . expressionEPrime 
                        | '-' expressionT . expressionEPrime 
                        | epsilon
 */
void parseExpressionEPrime(){
    int token = get_next_unconsumed_token();
    switch (token)
    {
    case ADD_TOKEN://add case
        //consume the token ADD_TOKEN
        get_next_token();
        setOperationExpression(AddExpression);
        
        parseExpressionT();
        parseExpressionEPrime();
        break;
    case SUB_TOKEN://sub case
        //consume the token SUB_TOKEN
        get_next_token();
        setOperationExpression(SubExpression);
        
        parseExpressionT();
        parseExpressionEPrime();
        break;
    default:
        // EPSILON case
        break;
    }
}

/**
 * `parseExpressionE` is a function that parses an expression, and it does so by first setting the
 * operation to load, then parsing an expression T, and then parsing an expression E prime.
 * 
 * expressionE = expressionT . expressionEPrime
 */
void parseExpressionE(){
    //set to load, (start of expression)
    setOperationExpression(LoadExpression);
    parseExpressionT();
    parseExpressionEPrime();
}

/* === END OF EXPRESSIONS === */

/**
 * It parses an int declaration
 */
void parseInt(){
    // get the next token
    int tok = get_next_token();
    // if the reading token is not an identifiers, it's doesn't match with the grammar
    if(tok != IDENT_TOKEN){
        errorHandler(ERROR_IDENTIFIER_EXPECTED);
    }
    // name add of the identifiers in the current var table (and check if it already exist)
    int line = addInVarTable();
    //parsing of equals token
    tok = get_next_token();
    if(tok != ASSIGN_TOKEN){
        errorHandler(ERROR_ASSIGNIMENT_EXPECTED);
    }  
    //TODO cas int vari;

    parseExpressionE();
    
    //parding of the semicolon
    tok = get_next_token();
    if(tok != SEMICOLON_TOKEN){errorHandler(ERROR_SEMICOLON_EXPECTED);}

    //generate code store at the adress of the name in position line in the table
    // The value in the akku must be the value to store

    //load the value name at line in the name buffer
    loadVariableValue(line);
    generateStoreCode();
    
    //for debug, print on the output the strored value
    generatePrintCode();

    //for debug
    // printVariableTable();
    // printImmediateTable();
}

/**
 * It parses an int declaration
 */
void parseIdent(){
    // name add of the identifiers in the current var table (and check if it already exist)
    int line = addInVarTable();
    //parsing of equals token
    int tok = get_next_token();
    if(tok != ASSIGN_TOKEN){
        errorHandler(ERROR_ASSIGNIMENT_EXPECTED);
    }  
    //TODO cas int vari;

    parseExpressionE();
    
    //parding of the semicolon
    tok = get_next_token();
    if(tok != SEMICOLON_TOKEN){errorHandler(ERROR_SEMICOLON_EXPECTED);}

    //generate code store at the adress of the name in position line in the table
    // The value in the akku must be the value to store

    //load the value name at line in the name buffer
    loadVariableValue(line);
    generateStoreCode();
    
    //for debug, print on the output the strored value
    generatePrintCode();

    //for debug
    // printVariableTable();
    // printImmediateTable();
}

void parseif();
void parseDo();
void parseWhile();

// todo better imbrication of while
void parseCondition(int isWhile, int nbIf,int numCond){ // Fonction used to parse the conditional statement in an 'if' or 'while' statement
    parseExpressionE(); //Parsing the expression directtly after openning the brackets, without consuming the next token that is not taken into account as an expression
    int next = get_next_unconsumed_token(); // Getting the next token after the first expression after the bracket. This token can either be a closing bracket or a comparator
    switch (next)
    {
    case EQ_TOKEN: // In case we have a comparator, we consume it and parse the next expression which it compares
        next = get_next_token(); // consume the token

        //store in the tmp variable the first part of the condition
        if(isWhile){
            loadWhileTmp(1);
        }else{
            loadIfTmp(1);
        }
        
        generateStoreCode();

        parseExpressionE(); //Parsing the expression (normally gives an error if it's not correctly written) and ends just after consuming the last token before the closing brackets

        //store in the tmp variable the first part of the condition
        if(isWhile){
            loadWhileTmp(2);
        }else{
            loadIfTmp(2);
        }
        generateStoreCode();

        if(isWhile){
            generateWhileEQCondition(nbIf,numCond);
        }else{
            generateEQCondition(nbIf,numCond);
        }
        break;
    case NEQ_TOKEN: //Comparator case
        next = get_next_token(); // consume the token

        //store in the tmp variable the first part of the condition
        if(isWhile){
            loadWhileTmp(1);
        }else{
            loadIfTmp(1);
        }
        generateStoreCode();

        parseExpressionE(); //Parsing the expression (normally gives an error if it's not correctly written) and ends just after consuming the last token before the closing brackets

        //store in the tmp variable the first part of the condition
        if(isWhile){
            loadWhileTmp(2);
        }else{
            loadIfTmp(2);
        }
        generateStoreCode();

        if(isWhile){
            generateWhileNEQCondition(nbIf,numCond);
        }else{
            generateNEQCondition(nbIf,numCond);
        }
        break;
    case LESS_TOKEN: //Comparator case
        next = get_next_token(); // consume the token LESS_EQ_TOKEN 

        //store in the tmp variable the first part of the condition
        if(isWhile){
            loadWhileTmp(1);
        }else{
            loadIfTmp(1);
        }
        generateStoreCode();

        parseExpressionE(); //Parsing the expression (normally gives an error if it's not correctly written) and ends just after consuming the last token before the closing brackets
        
        //store in the tmp variable the first part of the condition
        if(isWhile){
            loadWhileTmp(2);
        }else{
            loadIfTmp(2);
        }
        generateStoreCode();

        // do the sub
        if(isWhile){
            generateWhileLessCondition(nbIf,numCond);
        }else{
            generateLessCondition(nbIf,numCond);
        }
        break;
    case MORE_TOKEN: //Comparator case
        next = get_next_token(); // consume the token LESS_EQ_TOKEN 

        //store in the tmp variable the first part of the condition
        if(isWhile){
            loadWhileTmp(1);
        }else{
            loadIfTmp(1);
        }
        generateStoreCode();

        parseExpressionE(); //Parsing the expression (normally gives an error if it's not correctly written) and ends just after consuming the last token before the closing brackets
        
        //store in the tmp variable the first part of the condition
        if(isWhile){
            loadWhileTmp(2);
        }else{
            loadIfTmp(2);
        }
        generateStoreCode();

        // do the sub
        if(isWhile){
            generateWhileMoreCondition(nbIf,numCond);
        }else{
            generateMoreCondition(nbIf,numCond);
        }
        break;
    case MORE_EQ_TOKEN: //Comparator case
        next = get_next_token(); // consume the token LESS_EQ_TOKEN 

        //store in the tmp variable the first part of the condition
        if(isWhile){
            loadWhileTmp(1);
        }else{
            loadIfTmp(1);
        }
        generateStoreCode();

        parseExpressionE(); //Parsing the expression (normally gives an error if it's not correctly written) and ends just after consuming the last token before the closing brackets
        
        //store in the tmp variable the first part of the condition
        if(isWhile){
            loadWhileTmp(2);
        }else{
            loadIfTmp(2);
        }
        generateStoreCode();

        // do the sub
        if(isWhile){
            generateWhileMoreEqualCondition(nbIf,numCond);
        }else{
            generateMoreEqualCondition(nbIf,numCond);
        }
        break;
    case LESS_EQ_TOKEN: // a - b >= 0 (so c is activate)
        next = get_next_token(); // consume the token LESS_EQ_TOKEN 

        //store in the tmp variable the first part of the condition
        if(isWhile){
            loadWhileTmp(1);
        }else{
            loadIfTmp(1);
        }
        generateStoreCode();

        parseExpressionE(); //Parsing the expression (normally gives an error if it's not correctly written) and ends just after consuming the last token before the closing brackets
        
        //store in the tmp variable the first part of the condition
        if(isWhile){
            loadWhileTmp(2);
        }else{
            loadIfTmp(2);
        }
        generateStoreCode();

        // do the sub
        if(isWhile){
            generateWhileLessEqualCondition(nbIf,numCond);
        }else{
            generateLessEqualCondition(nbIf,numCond);
        }
        break;
    case CLOSE_PAR_TOKEN: //In this case, it is a comparator, and we're directly closing our conditional statement. This statement is akin to something like 'if(1)', with 1 being an expression
        break;
    default:
        errorHandler(ERROR_WRONG_COMPARISON);
    }
}

int parseConditionBoolean(int isWhile,int nbIf,int numCond){// Condition | !ConditionBoolean | Condition && ConditionBoolean | Condition || ConditionBoolean
    int next = get_next_unconsumed_token();
    if(next == NOT_TOKEN){
        next = get_next_token();//consume the !
        parseConditionBoolean(isWhile,nbIf,numCond);
        if(isWhile){
            generateWhileCondBoolNot(nbIf,numCond);
        }else{
            generateCondBoolNot(nbIf,numCond);
        }
        numCond += 1;
    }else{//read a condition
        parseCondition(isWhile,nbIf,numCond);
        next = get_next_unconsumed_token();
        if(next == AND_TOKEN){
            next = get_next_token();//consume the &&
            numCond+=1;
            if(isWhile){
                generateWhileCondBoolAnd(nbIf,numCond);
            }else{
                generateCondBoolAnd(nbIf,numCond);
            }
            parseConditionBoolean(isWhile,nbIf,numCond);
            return numCond;
        }else if(next == OR_TOKEN){
            next = get_next_token();//consume the ||
            numCond+=1;
            parseConditionBoolean(isWhile,nbIf,numCond);
            if(isWhile){
                generateWhileCondBoolOr(nbIf,numCond);
            }else{
                generateCondBoolOr(nbIf,numCond);
            }
            return numCond;
        }else if(next == CLOSE_PAR_TOKEN){
            //nothing else
        }else{
            printf("WUT %d\n",next);
            // TODO error case ??
        }   
    }
    return numCond;
    //here the generated code got a conditionnal jump with the above condition
    //generateJumpCode(nbIf);
}

void parseBody(){
    int next = get_next_token();
    while(next != CLOSE_BRACK_TOKEN){ //Iterating on the body as long as we haven't found the closing the '}' token
        switch(next){
            case WHILE_TOKEN:
                parseWhile();
                break;
            case IF_TOKEN:
                parseif();
                break;
            case DO_TOKEN:
                parseDo();
                break;
            case INT_TOKEN:
                parseInt();
                break;
            case IDENT_TOKEN:
                parseIdent();
                break;
            default: //todo faire cas
                return;
        }
        next = get_next_token();
    }
}

void parseDo(){
    int next = get_next_token(); // Opening curly bracket token
    if(next != OPEN_BRACK_TOKEN){
        errorHandler(ERROR_OPEN_BRACKET_EXPECTED);
    }
    parseBody(); //parsing the body of this statement
    next = get_next_token(); //After having closed, we get to the conditional statement
    if(next != WHILE_TOKEN){
        errorHandler(ERROR_WHILE_TOKEN_DO_EXPECTED);
    } 
    parseConditionBoolean(0,0,0);//TODO Modifier //Parsing the conditional statement in the while
}

int nbIf = 0;
void parseif(){ //Function used to parse the if statement
    int next = get_next_token(); //Getting the first token normally is the open bracket or a negation
    if (next != OPEN_PAR_TOKEN){ // if no (, then error
        errorHandler(ERROR_OPEN_PARENTESIS_EXPECTED);
    }
    
    int conditionNumber = parseConditionBoolean(0,nbIf,0); //Parsing the conditional statement in the if, consumming the )

    generateCondBoolToCond(nbIf,conditionNumber);

    nbIf ++;
    
    next = get_next_token(); //Getting the first token normally is the close bracket
    if (next != CLOSE_PAR_TOKEN){ // if no ), then error
        errorHandler(ERROR_CLOSE_PARENTESIS_EXPECTED);
    }

    //chek the start of the if statement zone
    next = get_next_token();
    if(next != OPEN_BRACK_TOKEN){
        errorHandler(ERROR_OPEN_BRACKET_EXPECTED);
    }

    //generate the start else tag :
    generateJumpStartIfTag(nbIf-1);
    //parse and generate code until see the corresponding close bracket, consume the bracket
    parseBody();

    //Set the endif tag
    generateJumpStartElseTag(nbIf - 1);//TODO faire qu'on puisse imbriqué les if

    //check if there is an else statement 
    next = get_next_unconsumed_token();
    if(next == ELSE_TOKEN){
        //TODO faire le jump si on est passé dans le if !
        next = get_next_token(); //consume else_token
        next = get_next_token(); //check if there is a braket,
        if(next != OPEN_BRACK_TOKEN){
            errorHandler(ERROR_OPEN_BRACKET_EXPECTED);
        }
        parseBody();
    }else {
        //if no code, just jump to the end tag
        generateJumpEndIfCode(nbIf - 1);
    }

    generateJumpEndIfTag(nbIf - 1);
}

int nbWhile = 0;
void parseWhile(){ //Function to parse the while statement
    int next = get_next_token();
    if(next != OPEN_PAR_TOKEN){
        errorHandler(ERROR_OPEN_PARENTESIS_EXPECTED);
    }

    generateTagStartWhile(nbWhile);

    int conditionNumber = parseConditionBoolean(1,nbWhile,0); //Parsing the conditional statement in the while, consumming the )
    
    generateWhileCondBoolToCond(nbIf,conditionNumber);

    nbWhile++;

    next = get_next_token(); //Getting the first token normally is the close bracket
    if (next != CLOSE_PAR_TOKEN){ // if no ), then error
        errorHandler(ERROR_CLOSE_PARENTESIS_EXPECTED);
    }

    next = get_next_token();
    if(next != OPEN_BRACK_TOKEN){
        errorHandler(ERROR_OPEN_BRACKET_EXPECTED);
    }

    //generate the start else tag
    generateJumpStartWhileTag(nbWhile-1);
    //parse and generate code until see the corresponding close bracket and consume the bracket
    parseBody();

    generateJumpStartWhile(nbWhile-1);

    //Set the endif tag
    //generateJumpStartElseTag(nbWhile - 1);//TODO faire qu'on puisse imbriqué les if
    
    //Jump back to the start else tag
   
}   

int parseFile(){
    int tok = get_next_token();
    while (tok != EOF_TOKEN && tok != ERROR_TOKEN)
    {
        /* code */
        switch (tok)
        {
            case DEFINE_TOKEN: //A file is either Define tokens
                /* code */
                parseDefine();
                break;
            
            case INT_TOKEN: // for the moment, parse int <id> = valueIm;
                parseInt();
                break;

            case DO_TOKEN:
                parseDo();
                break;

            case WHILE_TOKEN:
                parseWhile();
                break;

            case IF_TOKEN:
                parseif();
                break;
        
            // case INT_TOKEN: // Or globals définitions

            //     parseInt();
            //     break;
            // case CHAR_TOKEN: //

            //     break; 

            // FOR TESTINGS
            case INT_LITERAL_TOKEN:
                break;
            
            default:
                errorHandler(-1);
                break;
        }

        tok = get_next_token();
    }
    return -1;
}

/*========================================================*/

//for debug 
void printToken(int tok);

int main(int argc, char** argv){

    if(argc < 1){
        // PRINT HELP MESSAGE
        return EXIT_FAILURE;
    }

    //int verbose = 0;

    // If we put the -v arg then we activate verbose mode for debuging
    if(argc > 2 && argv[2][0] == '-' && argv[2][1] == 'v'){
        //verbose = 1;
    }
    /* Lexer phases */

    /* openning of the file from the params, MAJ global variable */
    file = fopen(argv[1], "r");

    /* opening the out.asm file that will contain the asm code of the program file */
    outfile = fopen("out.asm","w+");

    /*
    int c;
    do{
        c = get_next_token();

        if(verbose){ printToken(c); }

        if(c == ERROR_TOKEN){
            printf("Test Failed");
            return EXIT_FAILURE;
        }

    }while(c != EOF_TOKEN && c != ERROR_TOKEN);*/

    
    initDefineTable();
    initCodeGeneration();
    initiateImmediateTable();
    
    parseFile();

    //write the symboles tables
    writeSymboleTables();

    fclose(file);
    fclose(outfile);
    //printf("Test Passed \n");
    return EXIT_SUCCESS;

}



void printToken(int tok){
    switch (tok)
    {
    case IDENT_TOKEN:
        printf("Identifiant : %s\n",tokenCharValue);
        break;
    case IF_TOKEN:
        printf("If \n");
        break;
    case ELSE_TOKEN:
        printf("Else \n");
        break;
    case DO_TOKEN:
        printf("Do \n");
        break;
    case WHILE_TOKEN:
        printf("While \n");
        break;
    case INT_TOKEN:
        printf("int \n");
        break;
    case CHAR_TOKEN:
        printf("char \n");
        break;
    case RETURN_TOKEN:
        printf("return \n");
        break;
    case OPEN_PAR_TOKEN:
        printf("( \n");
        break;
    case CLOSE_PAR_TOKEN:
        printf(") \n");
        break;
    case OPEN_BRACK_TOKEN:
        printf("{ \n");
        break;
    case CLOSE_BRACK_TOKEN:
        printf("} \n");
        break;
    case OPEN_SQUARE_BRACK_TOKEN:
        printf("[ \n");
        break;
    case CLOSE_SQUARE_BRACK_TOKEN:
        printf("] \n");
        break;
    case SEMICOLON_TOKEN:
        printf("; \n");
        break;
    case ADD_TOKEN:
        printf("+ \n");
        break;
    case SUB_TOKEN:
        printf("- \n");
        break;
    case DIV_TOKEN:
        printf("/ \n");
        break;
    case MOD_TOKEN:
        printf("modulo \n");
        break;
    case STAR_TOKEN:
        printf("* \n");
        break;
    case ADR_TOKEN:
        printf("& \n");
        break;
    case NOT_TOKEN:
        printf("! \n");
        break;
    case AND_TOKEN:
        printf("&& \n");
        break;
    case OR_TOKEN:
        printf("|| \n");
        break;
    case EQ_TOKEN:
        printf("== \n");
        break;
    case NEQ_TOKEN:
        printf("!= \n");
        break;
    case LESS_TOKEN:
        printf("< \n");
        break;
    case LESS_EQ_TOKEN:
        printf("<= \n");
        break;
    case MORE_TOKEN:
        printf("> \n");
        break;
    case MORE_EQ_TOKEN:
        printf(">= \n");
        break;
    case ASSIGN_TOKEN:
        printf("= \n");
        break;
    case INT_LITERAL_TOKEN:
        printf("ENTIER : %d \n",tokenIntValue);
        break;
    case STRING_LITERAL_TOKEN:
        printf("STRING : %s \n",tokenCharValue);
        break;
    case CHAR_LITERAL_TOKEN:
        printf("CHAR : %c (%d) \n",tokenCharValue[0],tokenCharValue[0]);
        break;
    case ERROR_TOKEN:
        printf("ERRORRRRR !!!!!!!!!\n");
        break;
    case EOF_TOKEN:
        printf("EOF !\n");
        break;
    case DOT_TOKEN:
        printf("DOT \n");
        break;
    case DEFINE_TOKEN:
        printf("#define \n");
        break;
    default:
        break;
    }
}
